#include "bmp.h"

static const uint16_t SIGNATURE = 0x4d42;
static const uint32_t HEADER_INFO_SIZE = 40;
static const uint16_t BITS_PER_PIXEL = 24;

static inline uint32_t get_padding(uint32_t width) { return (4-(width*sizeof(struct pix)%4))%4; }

static struct header generate_header(const struct size dims) {
    const uint32_t head_dimensions = sizeof(struct header);
    const uint32_t img_dimensions = sizeof(struct pix)
                              * dims.height * (dims.width + get_padding(dims.width));
    const uint32_t file_dimensions = head_dimensions + img_dimensions;

    struct header new_header;
    new_header.bfType = SIGNATURE;
    new_header.bfileSize = file_dimensions;
    new_header.bfReserved = 0;
    new_header.bOffBits = head_dimensions;
    new_header.biSize = HEADER_INFO_SIZE;
    new_header.biWidth = dims.width;
    new_header.biHeight = dims.height;
    new_header.biPlanes = 1;
    new_header.biBitCount = BITS_PER_PIXEL;
    new_header.biCompression = 0;
    new_header.biSizeImage = img_dimensions;
    new_header.biXPelsPerMeter = 0;
    new_header.biYPelsPerMeter = 0;
    new_header.biClrUsed = 0;
    new_header.biClrImportant = 0;
    return (new_header);
}

static enum read_status read_header(FILE *in, struct header *head) {
    enum read_status temp = READ_OK;
    if (fread(head, sizeof(struct header), 1, in) < 1 || head->biSize != HEADER_INFO_SIZE || head->biPlanes != 1 ||
        head->biBitCount != BITS_PER_PIXEL || head->biCompression != 0)
        temp = READ_INVALID_HEADER;
    else if (head->bfType != SIGNATURE)
        temp = READ_INVALID_SIGNATURE;
    return temp;
}

static enum read_status read_image(FILE *in, struct image *img) {
    enum read_status temp = READ_OK;
    const struct size dims = img->dims;
    for (uint32_t i = 0; i < dims.height; i++) {
        if (fread(&img->data[i * dims.width], sizeof(struct pix), dims.width, in) < dims.width || fseek(in, get_padding(dims.width), SEEK_CUR) != 0) {
            temp = READ_INVALID_BITS;
            break;
        }
    }
    return temp;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    enum read_status temp = READ_OK;

    struct header head = {0};

    if ((temp = read_header(in, &head)) != 0)
        return temp;

    if (fseek(in, head.bOffBits, SEEK_SET) != 0)
        return READ_INVALID_BITS;

    *img = image_create(size_create(head.biWidth, head.biHeight));

    if ((temp = read_image(in, img)) != 0)
        image_destroy(img);
    return temp;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    enum write_status temp = WRITE_OK;

    const struct header head = generate_header(img->dims);
    if (fwrite(&head, sizeof(struct header), 1, out) < 1)
        return WRITE_ERROR;

    const struct size dims = img->dims;
    for (uint32_t o = 0, i = 0; i < dims.height; i++) {
        if (fwrite(&img->data[i * dims.width], sizeof(struct pix), dims.width, out)
            < dims.width || fwrite(&o, get_padding(dims.width), 1, out) < 1) {
            temp = WRITE_ERROR;
            break;
        }
    }
    return temp;
}
