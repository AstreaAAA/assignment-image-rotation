//
// Created by muha7 on 01.11.2022.
//
#include "image.h"
#include <stdlib.h>



static inline size_t get_size(struct size dims) {
    return dims.width * dims.height * sizeof(struct pix);
}

struct image image_create(struct size dims)
{
    struct image new_image;
    new_image.dims= dims;
    new_image.data = malloc(get_size(dims));
    return new_image;
}

void image_destroy(struct image * img) { free(img->data); }
