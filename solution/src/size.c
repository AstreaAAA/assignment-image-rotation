//
// Created by muha7 on 01.11.2022.
//
#include "size.h"

struct size size_create(uint32_t width, uint32_t height) {
    return (struct size) { width, height };
}

struct size size_reverse(struct size dims) {
    return (struct size) { dims.height, dims.width };
}
