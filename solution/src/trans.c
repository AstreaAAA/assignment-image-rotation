#include "stdio.h"
#include "trans.h"


struct image rotate_left(struct image const src) {
    struct size dims = size_reverse(src.dims);
    struct image img = image_create(dims);
    printf("%d %d", dims.width, dims.height);
    for (uint32_t i = 0; i < src.dims.height; i = i + 1) {
        for (uint32_t j = 0; j < src.dims.width; j = j + 1) {
            img.data[j* img.dims.width +(img.dims.width-i-1)] =
                    src.data[i* src.dims.width+j];
        }
    }
    return img;
}
