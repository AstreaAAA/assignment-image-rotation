#include "bmp.h"
#include "file.h"
#include "image.h"
#include "trans.h"
#include <stdio.h>
#define EXECUTABLE "image-trans"

static const char *r_msg[] = {
        [READ_INVALID_SIGNATURE] ="wrong bitmap signature",
        [READ_INVALID_BITS] = "wrong number of bits",
        [READ_INVALID_HEADER] = "wrong bitmap header"
};
static const char *w_msg[] = {[WRITE_ERROR] = "Error"};
void usage(void) {
    fprintf(stderr,
            "Usage: ./" EXECUTABLE " <source-image> <trans-image>\n");
}
int main(int argc, char *argv[]) {
    if (argc != 3) usage();
    FILE *in, *out;
    if (file_open(&in, argv[1], "rb") != 0) {

    }
    if (file_open(&out, argv[2], "wb") != 0) {
        file_close(&in);
    }
    struct image source = {0};
    enum read_status a = from_bmp(in, &source);
    file_close(&in);
    if (a != READ_OK) {
        fprintf(stderr, "%s\n", r_msg[a]);
        return a;
    }
    struct image transformed = rotate_left(source);
    enum write_status b = to_bmp(out, &transformed);
    file_close(&out);
    if (b != WRITE_OK) {
        image_destroy(&source);
        image_destroy(&transformed);
        fprintf(stderr, "%s\n", w_msg[b]);
        return b;
    }
    image_destroy(&source);
    image_destroy(&transformed);
    return 0;
}
