//
// Created by muha7 on 01.11.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_SIZE_H
#define ASSIGNMENT_IMAGE_ROTATION_SIZE_H
#include <stdint.h>

struct size {
    uint32_t width, height;
};

struct size size_create(uint32_t width, uint32_t height);
struct size size_reverse(struct size dims);
#endif //ASSIGNMENT_IMAGE_ROTATION_SIZE_H
