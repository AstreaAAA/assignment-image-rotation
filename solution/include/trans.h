

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANS_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANS_H
#include "image.h"

struct image rotate_left(struct image src);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANS_H
