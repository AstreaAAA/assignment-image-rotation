//
// Created by muha7 on 31.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include "pix.h"
#include "size.h"


struct image {
    struct size dims;
    struct pix *data;
};

struct image image_create(struct size dims );
void image_destroy(struct image * img);


#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
