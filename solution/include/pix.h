//
// Created by muha7 on 01.11.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_PIX_H
#define ASSIGNMENT_IMAGE_ROTATION_PIX_H
#include <stdint.h>

struct pix { uint8_t r, g, b; };
#endif //ASSIGNMENT_IMAGE_ROTATION_PIX_H
